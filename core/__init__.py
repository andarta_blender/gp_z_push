# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from mathutils import Vector, Matrix
import numpy as np
from itertools import combinations
import time

from ..common.utils import register_classes, unregister_classes
from ..common.gp_utils import get_geo, get_parent_collection_list, get_layer_collection
from ..common import timer, blender_ui

from .stroke_groups import get_stroke_groups, update_conflict_archives, shapely_ok
from .conflict_groups import GPObjectConflict, get_conflict
from . import events


conflicts = None

conflict_list = None
last_conflict_update = None
update_period = 10.0
last_frame = 0

last_conflict = None


def prefs() :
    module_name = __name__.replace(".core", "")
    return bpy.context.preferences.addons[module_name].preferences


def move_obj(axis, increment):
    ob = bpy.context.object
    axis_array = ['X', 'Y', 'Z']
    i = axis_array.index(axis)
    move = Vector((0.0, 0.0, 0.0))
    move[i] += increment
    move.rotate(ob.rotation_euler)
    ob.location += move
    if bpy.context.scene.tool_settings.use_keyframe_insert_auto :
        ob.keyframe_insert('location', index= i, frame= bpy.context.scene.frame_current)


def parent_hidden(obj) :
    for col in get_parent_collection_list(obj) :
        lc = get_layer_collection(col)
        if lc is not None :
            if lc.hide_viewport or lc.exclude :
                return True
    return False

def filter_invalid_shapes(conflicts) :
    valid_conflicts = []
    
    for c in conflicts :
        try : 
            c[0].get_shape()
            c[1].get_shape()
            valid_conflicts.append(c)
        except :
            print("Warning : %s was excluded from conflicts, could not retrieve a valid shape." % str(c))
    
    return valid_conflicts


def zfight_check() :

    silent = True
    def sprint(*args) :
        if not silent :
            print(*args)

    sprint("\nCh-ch-check your privileges !")

    t = timer.Timer("1. Zfight check")
    t1 = timer.Timer("1.1 Init")

    scene = bpy.context.scene
    gp_objects = [o for o in scene.objects if type(o.data) is bpy.types.GreasePencil]
    sprint("%d gp objects found." % len(gp_objects))

    stroke_groups = []
    viewlayer_object_names = [o.name for o in bpy.context.window.view_layer.objects]

    for o in gp_objects :
        if not o.hide_get() and "HLP" not in o.name and o.name in viewlayer_object_names :
            if not parent_hidden(o) :
                stroke_groups += get_stroke_groups(o)

    sprint("%d stroke groups." % len(stroke_groups))

    t1.next("1.2 Combinations")

    conflicts = combinations(stroke_groups, 2)
    
    t1.next("1.3 Name match")

    conflicts = [c for c in conflicts if not c[0].match_name(c[1])]
    sprint("%s conflicts after name match." % len(conflicts))
    
    t1.next("1.5 Y match")

    conflicts = [c for c in conflicts if c[0].match_Y(c[1])]
    sprint("%s conflicts after Y match." % len(conflicts))
    
    t1.next("1.6 Hitbox match")

    conflicts = [c for c in conflicts if c[0].match_hitbox(c[1])]
    sprint("%s conflicts after hitbox match." % len(conflicts))
    
    t1.next("1.7 Shape match")

    conflicts = filter_invalid_shapes(conflicts)
    conflicts = [c for c in conflicts if c[0].match_shape(c[1])]
    sprint("%s conflicts after hitbox match." % len(conflicts))

    t1.stop()
    t.stop()

    if not silent :
        timer.print_totals()
    
    timer.reset_totals()

    return conflicts


def get_conflict_list(force=False) :
    global conflict_list

    # Return cached conflict list if update is too frequent 
    # to avoid taking up too much ressource

    frame_n = bpy.context.scene.frame_current

    if force or conflict_list is None or events.update_needed() :
        conflicts = zfight_check()
        conflict_list = conflict_groups.group_conflicts(conflicts, frame_n)

    return conflict_list


def update(context=None) :
    context = context if context is not None else bpy.context
    depsgraph = context.evaluated_depsgraph_get()
    depsgraph.update()


def local_spaces(context=bpy.context) :
    areas = [area for area in context.screen.areas if area.type =='VIEW_3D']
    return [a.spaces[0] for a in areas if a.spaces[0].local_view is not None]


def is_local_view(context=bpy.context) :
    return len(local_spaces(context)) > 0


def set_local_view(status, context=None) :
    context = context if context is not None else bpy.context
    areas = [area for area in context.screen.areas if area.type =='VIEW_3D']
    for area in areas :
        is_local = area.spaces[0].local_view is not None
        if is_local != status :
            with context.temp_override(area=area):
                bpy.ops.view3d.localview()


def edit_local_view_objects(new_object_names, context=bpy.context) :
    for space in local_spaces(context) :
        objects = []
        for o in context.scene.objects :
            is_local = o.name in new_object_names
            o.local_view_set(space, is_local)
            o.select_set(is_local)
            if is_local :
                objects.append(o)
        center_space(objects, space)


def center_view(new_object_names, context=bpy.context) :
    for space in local_spaces(context) :
        objects = [o for o in context.scene.objects if o.name in new_object_names]
        center_space(objects, space)


def center_space(objects, space) :
    center_pos = np.mean([np.array(o.matrix_world.to_translation()) for o in objects], axis=0)
    bpy.context.scene.cursor.location = Vector(center_pos)
    space.region_3d.view_location = Vector(center_pos)


def get_last_conflict() :
    global last_conflict
    return last_conflict


def store_last_conflict(conflict) :
    global last_conflict
    last_conflict = conflict


def jump(conflict, context) :

    if conflict is None :
        blender_ui.show_message("No conflict found !")
        return
    
    if context.scene.zfight_localview :
        set_local_view(True)
        edit_local_view_objects(conflict.object_names(), context)
    
    else :
        center_view(conflict.object_names(), context)
    
    conflict.select_objects()
    store_last_conflict(conflict)


def next_zfight_frame(reverse=False) :
    scene = bpy.context.scene

    n = scene.frame_current
    frame_list = list(range(n+1, scene.frame_end+1)) + [n] + list(range(scene.frame_start, n))
    
    if reverse :
        frame_list = reversed(frame_list)

    for i in frame_list :
        scene.frame_set(i)
        update()
        conflicts = zfight_check()
        if len(conflicts) > 0 :
            return i


def next_frame_conflict(conflicts, reverse=False) :
    next_frame = next_zfight_frame(reverse=reverse)
    if next_frame is not None :
        conflicts = get_conflict_list()
        if not reverse :
            return conflicts[0]
        else :
            return conflicts[-1]


def default_next_conflict(conflicts, reverse=False) :
    
    if len(conflicts) > 0 :
        return conflicts[0]

    else :
        next_frame_conflict(conflicts, reverse=reverse)


def next_conflict(reverse=False) :
    global last_conflict
    conflicts = get_conflict_list()

    # No last_conflict stored
    if last_conflict is None :
        return default_next_conflict(conflicts, reverse=reverse)

    next_c = last_conflict.prev() if reverse else last_conflict.next()

    # Last conflict found, but no next conflict in this frame
    if next_c is None :
        return next_frame_conflict(conflicts, reverse=reverse)

    next_c = next_c.update(conflicts)

    # New conflict still exists in updated conflict list
    if next_c is not None :
        return next_c

    return default_next_conflict(conflicts, reverse=reverse)
    

class ZfightMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_zfight_menu"
    bl_label = "Zfight check"

    def draw(self, context):
        if not shapely_ok() :
            row = self.layout.row()
            row.label(text="Warning : zfight check is disabled because shapely is not installed on your machine.", icon="ERROR")

        row = self.layout.row()
        row.prop(bpy.context.scene, "zfight_autocheck", text="Real-time refresh")

        row = self.layout.row()
        row.prop(bpy.context.scene, "zfight_tolerance", text="Tolerance")

        self.layout.separator()

        if get_last_conflict() is not None :
            row = self.layout.row()
            status = "ON" if context.scene.zfight_localview else "OFF"
            row.operator("gpencil.toggle_local_conflict_display", text="Local conflict view : %s" % status, icon="TRACKER")

        row = self.layout.row()
        row.operator("gpencil.prev_zfight", text="Previous Z-fight", icon="REW")
        
        row = self.layout.row()
        row.operator("gpencil.next_zfight", text="Next Z-fight", icon="FF")

        # row = self.layout.row()
        # row.operator("gpencil.zfight_analysis", text="Test", icon="MODIFIER")

        self.layout.separator()

        for c in get_conflict_list() :
            row = self.layout.row()
            row.operator("gpencil.show_zfight", text=c.name).name = c.name






classes = [
    ZfightMenu,
]

def register() :
    register_classes(classes)
    bpy.types.Scene.zfight_autocheck = bpy.props.BoolProperty(name="Real time z-fight check activation status", default=False)
    bpy.types.Scene.zfight_localview = bpy.props.BoolProperty(name="Use local view for conflicts display", default=False)
    bpy.types.Scene.zfight_tolerance = bpy.props.FloatProperty(name="Z-fight Tolerance", default=0.00099, step=0.001, max=1.0, min=0.0)

def unregister() :
    unregister_classes(classes)
    del bpy.types.Scene.zfight_tolerance
    del bpy.types.Scene.zfight_localview
    del bpy.types.Scene.zfight_autocheck